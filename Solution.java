//Решето Эратосфена (Sieve of Eratosthenes)

class Solution {
    public int countPrimes(int n) {
        boolean [] numbers = new boolean[n];
        int count = 0;
        for(int i =2; i<n; i++){
            if(numbers[i]==false){
                count++;
                for(int k = 2 ; i*k < n;k++){
                    numbers[i*k] = true;
                }
            }
        }
        return count;
    }
}
